window.onload = function () {

  // @ts-ignore
  if(Cookies.get("sessionId") == undefined || Cookies.get("sessionId") == "") {
    location.href = "index.html";
  }

  onHashChange(window.location.hash.substr(1));
};

window.onhashchange = function () {
  onHashChange(window.location.hash.substr(1));
};

/**
 * Function that should be executed when the hash changes
 * @param newhash The new hash, WITHOUT the '#' at the start
 */
function onHashChange(newhash: string) {
  if (newhash == "users") {
    // @ts-ignore
    loadComponent(userComponent)
  } else if (newhash == "dynamic") {
    // @ts-ignore
    loadComponent(dynamicContentComponent)
  } else {
    // @ts-ignore
    loadComponent(userComponent)
  }
}

function logout() {
  // @ts-ignore
  const sessionId = Cookies.get("sessionId");

  // @ts-ignore
  logOut(sessionId);

  // @ts-ignore
  Cookies.remove("sessionId");
  // @ts-ignore
  Cookies.remove("username");
  location.href = "index.html";
}