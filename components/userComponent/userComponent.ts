declare var iqwerty:any;
/**
 * INTERFACES
 */

interface User {
    id: number,
    username: string,
    admin: boolean
}

/**
 * COMPONENT CODE
 */
// @ts-ignore
class UsersComponentCode extends ComponentCode{

    main() {
        // @ts-ignore
        const sessionId = Cookies.get("sessionId");

        // @ts-ignore
        fetch(`${serverURL}/admin/getUserList?sessionId=${sessionId}`)
            .then(function(response: Response){
                if (response.status == 200) {
                    response.json().then(function(json: Array<User>){
                        for(const jsonEl of json) {
                            document.querySelector("#users-table tbody").appendChild(
                                new UserTableRow(jsonEl).build()
                            )
                        }
                    })
                } else {
                    response.text().then(function (error: string) {
                        if (
                            error == "ERR_NO_SUCH_SESSION" ||
                            error == "ERR_NO_SUCH_USER" ||
                            error == "ERR_UNAUTHORISED"
                        ) {
                            // @ts-ignore
                            logout()
                        }
                    })
                }
            });
        this.addRegistrationFormFunctionality();
    }

    private addRegistrationFormFunctionality(){
        const toastOptions = {
            settings: {
                duration: 2000
            }
        };

        const registerForm = document.getElementById("registerform") as HTMLFormElement;
        registerForm.addEventListener("submit", function(event){
            event.preventDefault();

            const username = (document.getElementById("registerform-username") as HTMLInputElement).value;
            const password = (document.getElementById("registerform-password") as HTMLInputElement).value;
            const confirmPassword = (document.getElementById("registerform-confirmpassword") as HTMLInputElement).value;
            const isAdmin = ((document.getElementById("registerform-admin") as HTMLInputElement).checked);

            if (password != confirmPassword) {
                // @ts-ignore
                iqwerty.toast.Toast("Wachtwoorden komen niet overeen", toastOptions);
                return false;
            } else if (password == "" || username == "") {
                // @ts-ignore
                iqwerty.toast.Toast("Vul alle velden in", toastOptions);
                event.preventDefault();
                return false;
            }

            // @ts-ignore
            const sessionId = Cookies.get("sessionId");

            const data = new FormData();
            data.append("username", username);
            data.append("password", password);
            data.append("admin", String(isAdmin));
            data.append("sessionId", sessionId);



            fetch(serverURL + "/register", {
                method: "post",
                body: data
            }).then(function(response: Response){
                if (response.status !== 200) {
                    response.text().then(function(text: string){
                        if (text == "ERR_USERNAME_TAKEN") {
                            iqwerty.toast.Toast("Gebruiker bestaat al", toastOptions);
                        }
                    });
                } else {
                    (document.getElementById("registerform-username") as HTMLInputElement).value = "";
                    (document.getElementById("registerform-password") as HTMLInputElement).value = "";
                    location.reload();
                }
            });
            return true;
        });
    }
}

/**
 * UTILITY CLASSES
 */
class UserTableRow {
    private readonly row: HTMLTableRowElement;

    constructor(user: User) {
        const toastOptions = {
            settings: {
                duration: 2000
            }
        };

        const row = document.createElement("tr") as HTMLTableRowElement;

        const idColumn = document.createElement("th") as HTMLTableHeaderCellElement;
        idColumn.scope = "row";
        idColumn.innerText = String(user.id);
        row.appendChild(idColumn);

        const usernameColumn = document.createElement("td") as HTMLTableCellElement;
        usernameColumn.innerText = user.username;
        row.appendChild(usernameColumn);

        const actionsRow = document.createElement("td") as HTMLTableCellElement;
        actionsRow.className = "user-action-row";

        const makeAdminButton = document.createElement("button") as HTMLButtonElement;
        let className = "btn btn-dark btn-outline-light fas user-action-btn";

        if (user.admin) {
            className += " fa-user-lock";
            makeAdminButton.title = "Maak normale gebruiker";
        } else {
            className += " fa-user-shield";
            makeAdminButton.title = "Maak administrator";
        }


        makeAdminButton.className = className;

        makeAdminButton.addEventListener("click", function () {
            this.disabled = true;

            let url: string;
            const onRequestComplete = function(el: HTMLButtonElement) {
                if (user.admin) {
                    el.className = el.className.replace("fa-user-shield", "fa-user-lock");
                    el.title = "Maak normale gebruiker";
                } else {
                    el.className = el.className.replace("fa-user-lock", "fa-user-shield");
                    el.title = "Maak admin";
                }
                el.disabled = false;
            };
            if (user.admin) {
                user.admin = false;
                // @ts-ignore
                url = serverURL + "/admin/users/makeNormal";

            } else {
                user.admin = true;
                // @ts-ignore
                url = serverURL + "/admin/users/makeAdmin";
            }
            // @ts-ignore
            const sessionId = Cookies.get("sessionId");

            const data = new FormData();
            data.append("sessionId", sessionId);
            data.append("userId", String(user.id));

            const button = this;

            // @ts-ignore
            fetch(url, {
                method: "POST",
                body: data
            }).then(function(response: Response) {
                if (response.status == 200) {
                    onRequestComplete(button);
                }
            });
        });

        const changePasswordButton = document.createElement("button") as HTMLButtonElement;
        changePasswordButton.className = "btn btn-dark btn-outline-light fas fa-key user-action-btn";
        changePasswordButton.title = "Verander wachtwoord";
        changePasswordButton.setAttribute("data-toggle", "modal");
        changePasswordButton.setAttribute("data-target", "#changePasswordModal");

        changePasswordButton.addEventListener("click", function () {

            document.getElementById("changePasswordModal-usernamebox").innerText = user.username;

            document.getElementById("changePasswordModal-submit").addEventListener("click", function(){
                const newPasswordEl = document.getElementById("changePasswordModal-newpassword") as HTMLInputElement;
                const confirmNewPasswordEl = document.getElementById("changePasswordModal-confirmnewpassword") as HTMLInputElement;

                const newPassword = newPasswordEl.value;
                const confirmNewPassword = confirmNewPasswordEl.value;

                if (newPassword == "") {
                    // @ts-ignore
                    iqwerty.toast.Toast("Voer een nieuw wachtwoord in", toastOptions);
                } else if (newPassword !== confirmNewPassword) {
                    // @ts-ignore
                    iqwerty.toast.Toast("Wachtwoorden komen niet overeen", toastOptions);
                } else {
                    const data = new FormData();
                    // @ts-ignore
                    data.append("sessionId", Cookies.get("sessionId"));
                    data.append("userId", String(user.id));
                    data.append("newpassword", newPassword);
                    // @ts-ignore
                    fetch(serverURL + "/admin/users/changePassword", {
                        method: "POST",
                        body: data
                    }).then(
                        function (response: Response) {
                            if (response.status == 200) {
                                newPasswordEl.value = "";
                                confirmNewPasswordEl.value = "";
                                //Close the modal by triggering the close button
                                document.getElementById("changePasswordModal-close").click();
                            }
                        }
                    )
                }

            });

        });

        const deleteButton = document.createElement("button") as HTMLButtonElement;
        deleteButton.className = "btn btn-dark btn-outline-danger fas fa-trash-alt user-action-btn";
        deleteButton.title = "Verwijder gebruiker";

        deleteButton.addEventListener("click", function(){
            if(!window.confirm("Weet je het zeker? De gebruiker \"" + user.username + "\" zal permanent verwijderd worden.")){
                return
            }
            this.disabled = true;
            // @ts-ignore
            const sessionId = Cookies.get("sessionId");

            const data = new FormData();
            data.append("sessionId", sessionId);
            data.append("userId", String(user.id));

            const el = this;

            // @ts-ignore
           fetch(serverURL + "/admin/users/remove", {
               method: "POST",
               body: data
           }).then(function(response: Response){
               if (response.status == 200) {
                   row.remove();
                   el.disabled = false;
               }
           });
        });

        // @ts-ignore
        if (user.username === Cookies.get("username")) {
            makeAdminButton.disabled = true;
            deleteButton.disabled = true;
        }

        actionsRow.appendChild(makeAdminButton);
        actionsRow.appendChild(changePasswordButton);
        actionsRow.appendChild(deleteButton);
        row.appendChild(actionsRow);


        this.row = row;
    }

    build(): HTMLTableRowElement {
        return this.row;
    }
}
