const serverURL = "https://augustus.romereis.toomuchram.net";

interface LoginResponse {
    username: string,
    userId: number,
    sessionId: string,
    admin: number
}

function logIn() {

    const toastOptions = {
        settings: {
            duration: 2000
        }
    };

    const username = (document.getElementById("username-field") as HTMLInputElement).value;
    const password = (document.getElementById("password-field") as HTMLInputElement).value;

    if (username == "") {
        // @ts-ignore
        iqwerty.toast.Toast("Voer een gebruikersnaam in", toastOptions);
        return
    } else if (password == "") {
        // @ts-ignore
        iqwerty.toast.Toast("Voer een wachtwoord in", toastOptions);
        return
    }

    const data = new FormData();
    data.append("username", username);
    data.append("password", password);

    // Get excursion ID
    // Address of the current window
    const address = window.location.search;
    // Returns a URLSearchParams object instance
    const parameterList = new URLSearchParams(address);

    data.append("excursionId", parameterList.get("excursionId"));

    fetch(serverURL + "/login", {
        method: "POST",
        body: data
    }).then(function(response: Response){
        if (response.status !== 200) {
            response.text().then(function(error :string){
                if (error == "ERR_NO_SUCH_USER") {
                    // @ts-ignore
                    iqwerty.toast.Toast("Deze gebruiker bestaat niet", toastOptions);
                } else if (error == "ERR_WRONG_PASSWORD") {
                    // @ts-ignore
                    iqwerty.toast.Toast("Fout wachtwoord", toastOptions);
                }
            });
        } else {
            response.json().then(function(json: LoginResponse) {
                if (json.admin === 0) {
                    // @ts-ignore
                    iqwerty.toast.Toast("Niet genoeg rechten", toastOptions);

                    logOut(json.sessionId)
                } else {
                    location.href = "console.html";

                    // @ts-ignore
                    Cookies.set("sessionId", json.sessionId);
                    // @ts-ignore
                    Cookies.set("username", json.username);
                }
            })
        }
    })
}

function logOut(sessionId: string) {
    const data = new FormData();
    data.append("sessionId", sessionId);

    fetch(serverURL + "/logout", {
            method: "POST",
            body: data
        }
    )
}
