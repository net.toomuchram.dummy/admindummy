declare var MediumEditor:any;
declare var AutoList:any;
declare var Cookies:any;

class DynamicContentComponent extends ComponentCode {
    private info_editor;
    private programme_editor;
    private teams_editor;
    private sessionId;
    private toastOptions = {
        settings: {
            duration: 2000
        }
    };

    main() {
        this.sessionId = Cookies.get("sessionId");
        const self = this

        const autolist = new AutoList();

        this.info_editor = new MediumEditor('#info-editor', {
            buttonLabels: 'fontawesome',
            extensions: {
                'autolist': autolist
            },
            toolbar: {
                buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'unorderedlist','orderedlist']
            }});
        this.getAndLoadContent("info", this.info_editor);
        document.getElementById("save-info-button").addEventListener("click", function(){
           const content = self.info_editor.getContent();
            self.saveContent("info", content);
        });

        this.programme_editor = new MediumEditor('#programme-editor', {
            buttonLabels: 'fontawesome',
            extensions: {
                'autolist': autolist
            },
            toolbar: {
                buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'unorderedlist','orderedlist']
            }});
        this.getAndLoadContent("programme", this.programme_editor);
        document.getElementById("save-programme-button").addEventListener("click", function(){
            const content = self.programme_editor.getContent();
            self.saveContent("programme", content);
        });

        this.teams_editor = new MediumEditor('#teams-editor', {
            buttonLabels: 'fontawesome',
            extensions: {
                'autolist': autolist
            },
            toolbar: {
                buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'unorderedlist','orderedlist']
            }});
        this.getAndLoadContent("teams", this.teams_editor);
        document.getElementById("save-teams-button").addEventListener("click", function(){
            const content = self.teams_editor.getContent();
            self.saveContent("teams", content);
        });

    }

    /**
     * Gets the current content from the server and puts it in the editor
     * @param contentName The name of the content to get from the Internet
     * @param editor The MediumEditor in which to put it
     */
    getAndLoadContent(contentName: string, editor: any) {
        const url = serverURL + "/dynamicContent?sessionId=" + this.sessionId + "&name=" + contentName;
        fetch(url).then(function (response: Response) {
                if (response.status == 200) {
                    response.text().then(function (responseText: string) {
                        editor.setContent(responseText)
                    })
                }
            }
        )
    }

    saveContent(contentName: string, content: string) {
        const url = serverURL + "/editDynamicContent";
        const data = new FormData();
        const self = this;
        data.append("name", contentName);
        data.append("content", content);
        data.append("sessionId", this.sessionId);

        fetch(url, {
            method: "POST",
            body: data
        }).then(function(response: Response){
            if (response.status == 200) {
                iqwerty.toast.Toast("Opgeslagen", self.toastOptions)
            } else if (response.status == 401) {
                iqwerty.toast.Toast("Fout bij het authoriseren", self.toastOptions)
            } else {
                iqwerty.toast.Toast("Mislukt", self.toastOptions)
            }
        })
    }
}